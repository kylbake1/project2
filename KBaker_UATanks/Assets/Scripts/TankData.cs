﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankData : MonoBehaviour
{
    // This section is public and customizable in the inspector for the designers
    public float moveSpeed; // in meters per seconds
    public float moveVolume; // this is the volume when the player moves
    public float reverseSpeed;  // this will move the player backwards
    public float turnSpeed = 180f; // in degrees per second
    public float turnVolume; // when the player turns, there will be sound for AI
    public float volume; // This is the sound for the NoiseMaler
    public float decayPerFrameDraw; // This is the sound that will decrease in NoiseMaker
    public float fireRate; // This will control how fast the player can shot per second
    public float bulletSpeed; // This how fast the bullet will travel
    public float bulletDestroyTime; // This is how long the bullet will last before being destroyed
    public float waitTillNextFire; // This will work with firerate to control shooting
    public float giveUpChaseDistance; // This is for AI give up
    public float closeEnoughDistance; // This is for AI CLose enough distance becuase it wont be exact

    // This section is private and is not customizable to the designers
    // - There is no private variables in this section -

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public enum InputScheme { WASD, arrowKeys};
    public enum ShootingScheme { space, rightShift}
    public InputScheme input = InputScheme.WASD;
    public ShootingScheme shoot = ShootingScheme.space;
    public TankMotor motor;
    public TankData data;
    public Rigidbody bulletPrefab;
    public Transform fireTransform;

    // Start is called before the first frame update
    void Start()
    {
        if (data == null)
        {
            data = gameObject.GetComponent<TankData>();
            motor = gameObject.GetComponent<TankMotor>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        switch (input)
        {
            case InputScheme.arrowKeys:
                if (Input.GetKey(KeyCode.UpArrow)) // this will move the player forward/drive
                {
                    motor.Move(data.moveSpeed);
                }
                if (Input.GetKey(KeyCode.DownArrow)) // this will move the player backwards/reverse
                {
                    motor.Move(-data.reverseSpeed);
                }
                if (Input.GetKey(KeyCode.RightArrow)) // this will turn the player right
                {
                    motor.Rotate(data.turnSpeed);
                }
                if (Input.GetKey(KeyCode.LeftArrow)) // this will turn the player left
                {
                    motor.Rotate(-data.turnSpeed);
                }
                break;
            case InputScheme.WASD:
                if (Input.GetKey(KeyCode.W)) // this will move the player forward/drive
                {
                    motor.Move(data.moveSpeed);
                }
                if (Input.GetKey(KeyCode.S)) // this will move the player backwards/reverse
                {
                    motor.Move(-data.reverseSpeed);
                }
                if (Input.GetKey(KeyCode.D)) // this will turn the player right
                {
                    motor.Rotate(data.turnSpeed);
                }
                if (Input.GetKey(KeyCode.A)) // this will turn the player left
                {
                    motor.Rotate(-data.turnSpeed);
                }
                break;
        }

        switch (shoot)
        {
            case ShootingScheme.space:
                if (Input.GetKey(KeyCode.Space))
                {
                    Shoot();
                }
                // this will control the firerate when moving the code, DONT FORGET.
                data.waitTillNextFire -= Time.deltaTime * data.fireRate;
                break;

            case ShootingScheme.rightShift:
                if (Input.GetKey(KeyCode.RightShift))
                {
                    Shoot();
                }
                data.waitTillNextFire -= Time.deltaTime * data.fireRate;
                break;
        }
    }

    public void Shoot()
    {
        if (data.waitTillNextFire <= 0)
        {
            Rigidbody bulletInstance;
            bulletInstance = Instantiate(bulletPrefab, fireTransform.position, fireTransform.rotation) as Rigidbody;
            bulletInstance.AddForce(fireTransform.forward * data.bulletSpeed);
            data.waitTillNextFire = 1;
        }
    }
}